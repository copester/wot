from mrjob.job import MRJob
import sequitur


class MRSequitur(MRJob):
    """A MapReduce implementation of Sequitur.

    Right now, this is just a batch system. It doesn't yet do any intra-task
    parallelization.
    """
    def mapper(self, _, line):
        output = sequitur.run(open(line).read())
        yield line, output
        

if __name__ == '__main__':
    MRSequitur.run()


